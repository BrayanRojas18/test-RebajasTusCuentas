export default function () {
  return {
    user: {},
    company: {
      logo_url: null,
    },
    modulos: [],
    misPermisos: [],

    notifications: [],
  };
}
