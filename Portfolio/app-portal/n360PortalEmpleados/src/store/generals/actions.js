export async function infoMenu({ state, commit }, role) {
  if (role !== "colaborador") return;
  await this._vm.$api
    .post("configuraciones/menu", { rol: role })
    .then((val) => {
      commit("cargarMenus", val.data);
    });
}
export async function loadConfiguracion({ state, commit }, empresa) {
  if (!Object.keys(empresa).length) return;
  commit("saveCompany", empresa);
}
