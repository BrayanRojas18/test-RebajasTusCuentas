export function saveCompany(state, company) {
  state.company = company;
}

export function saveUser(state, user) {
  state.user = user;
}
export function cargarMenus(state, menu) {
  state.misPermisos = menu.role;
}

export function Notifications(state, list) {
  state.notifications = list;
}
