function descargar(val) {
  window.saveAs(val, val.substring(val.lastIndexOf("/") + 1, val.length));
}
export default ({ Vue }) => {
  Vue.prototype.$download = descargar;
};
