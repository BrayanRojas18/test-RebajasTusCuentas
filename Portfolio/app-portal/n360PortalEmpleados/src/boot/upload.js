import env from "./env";
import toFormData from "./toFormData";
import httpAsync from "./httpAsync";
import { openURL } from "quasar";

export default ({ Vue }) => {
  Vue.prototype.$files = (path, url, params) => ({
    put(file) {
      return new Promise(async (resolve, reject) => {
        try {
          await httpAsync(path).post(
            toFormData({
              path: url,
              file,
            }),
            params
          );
          resolve(url);
        } catch (e) {
          reject(e);
        }
      });
    },
    get() {
      openURL(env.apiUrl + "/" + url);
    },
  });
};
