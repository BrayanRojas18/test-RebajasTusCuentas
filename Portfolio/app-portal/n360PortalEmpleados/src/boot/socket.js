import io from "socket.io-client";
import env from "./env";
import { axiosInstance } from "./axios";
import { LocalStorage } from "quasar";

let socket = io(env.socket);

export default ({ Vue, store }) => {
  socket.notify = function () {
    socket.emit("notify");
    socket.on("notify", async function (list) {
      let res = await axiosInstance.post("notificaciones/listado", {
        url: LocalStorage.getItem("selected").apiUrl,
        user: LocalStorage.getItem("user"),
      });
      store.commit("generals/Notifications", res.data);
    });
  };

  Vue.prototype.$socket = socket;
};
