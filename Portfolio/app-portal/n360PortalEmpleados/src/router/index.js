import Vue from "vue";
import VueRouter from "vue-router";

import routes from "./routes";

import { Notify, LocalStorage } from "quasar";

Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function ({ store }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  });

  Router.beforeEach((to, from, next) => {
    let role;
    let aproDpt = false;
    if (store.state.generals.user.hasOwnProperty("role"))
      role = store.state.generals.user.role;

    if (localStorage.getItem("aprobador_dpto"))
      aproDpt = localStorage.getItem("aprobador_dpto");

    if (to.path !== "/login" && !role) {
      return next("login");
    }
    if ((role && role == "colaborador") || aproDpt) {
      return next();
    } else {
      if (role && role !== "colaborador") {
        Notify.create({
          title: "Advertencia",
          message:
            "No tiene permiso para ingresar al portal de empleados, comuniquese con su administrador.",
          color: "red",
        });
        LocalStorage.clear();
        return next(false);
      }
    }

    next();
  });

  return Router;
}
