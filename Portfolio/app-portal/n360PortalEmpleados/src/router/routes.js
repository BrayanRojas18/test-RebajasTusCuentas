const routes = [
  {
    path: "/login",
    component: () => import("layouts/Login.vue"),
  },
  {
    path: "/panel",
    redirect: "/panel",
    component: () => import("layouts/Panel.vue"),
    children: [
      {
        path: "/panel",
        component: () => import("pages/Panel/Listado.vue"),
      },
      {
        path: "/panel/crear",
        component: () => import("pages/Panel/Crear.vue"),
      },
      {
        path: "/panel/editar/:id",
        component: () => import("pages/Panel/Crear.vue"),
      },
    ],
  },
  {
    path: "/",
    redirect: "/home",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      // APROBADORES
      {
        path: "/pendientes-empleados",
        component: () => import("src/pages/Aprobadores/Index.vue"),
        meta: {
          name: "APROBACIONES",
          icon: "done",
        },
      },
      {
        path: "/home",
        component: () => import("src/pages/Home.vue"),
        meta: {
          name: "INICIO",
          icon: "dashboard",
        },
      },
      {
        path: "/mi-departamento/lista",
        component: () => import("src/pages/Departamentos/Listado.vue"),
        meta: {
          name: "VER MI DEPARTAMENTO",
          permission: "10.2",
          icon: "visibility",
        },
      },
      {
        path: "/mis-roles/lista",
        component: () => import("src/pages/Roles/Listado.vue"),
        meta: {
          name: "VER MIS ROLES",
          permission: "10.3",
          icon: "visibility",
        },
      },
      // PRESTAMOS
      {
        path: "/mis-prestamos/solicitar",
        component: () => import("src/pages/Prestamos/Solicitar.vue"),
        meta: {
          name: "SOLICITAR PRÉSTAMOS",
          permission: "10.5",
          icon: "request_quote",
        },
      },
      {
        path: "/mis-prestamos/solicitar/:id",
        component: () => import("src/pages/Prestamos/Solicitar.vue"),
        meta: {
          name: "EDITAR PRÉSTAMOS",
          permission: "10.5",
          icon: "request_quote",
        },
      },
      {
        path: "/mis-prestamos/lista",
        component: () => import("src/pages/Prestamos/Listado.vue"),
        meta: {
          name: "VER MIS PRÉSTAMOS",
          permission: "10.5",
          icon: "visibility",
        },
      },
      // ANTICIPOS
      {
        path: "/mis-anticipos/solicitar",
        component: () => import("src/pages/Anticipos/Solicitar.vue"),
        meta: {
          name: "SOLICITAR ANTICIPOS",
          permission: "10.4",
          icon: "request_quote",
        },
      },
      {
        path: "/mis-anticipos/solicitar/:id",
        component: () => import("src/pages/Anticipos/Solicitar.vue"),
        meta: {
          name: "EDITAR ANTICIPOS",
          permission: "10.4",
          icon: "request_quote",
        },
      },
      {
        path: "/mis-anticipos/lista",
        component: () => import("src/pages/Anticipos/Listado.vue"),
        meta: {
          name: "VER MIS ANTICIPOS",
          permission: "10.4",
          icon: "visibility",
        },
      },

      // VACACIONES
      {
        path: "/mis-vacaciones/solicitar",
        component: () => import("src/pages/Vacaciones/Solicitar.vue"),
        meta: {
          name: "SOLICITAR VACACIONES",
          permission: "10.6",
          icon: "request_quote",
        },
      },
      {
        path: "/mis-vacaciones/solicitar/:id",
        component: () => import("src/pages/Vacaciones/Solicitar.vue"),
        meta: {
          name: "EDITAR VACACIONES",
          permission: "10.6",
          icon: "request_quote",
        },
      },
      {
        path: "/mis-vacaciones/lista",
        component: () => import("src/pages/Vacaciones/Listado.vue"),
        meta: {
          name: "VER MIS VACACIONES",
          permission: "10.6",
          icon: "visibility",
        },
      },
      // PERMISOS
      {
        path: "/mis-permisos/solicitar",
        component: () => import("src/pages/Permisos/Solicitar.vue"),
        meta: {
          name: "SOLICITAR PERMISOS",
          permission: "10.7",
          icon: "request_quote",
        },
      },
      {
        path: "/mis-permisos/solicitar/:id",
        component: () => import("src/pages/Permisos/Solicitar.vue"),
        meta: {
          name: "EDITAR PERMISOS",
          permission: "10.7",
          icon: "request_quote",
        },
      },
      {
        path: "/mis-permisos/lista",
        component: () => import("src/pages/Permisos/Listado.vue"),
        meta: {
          name: "VER MIS PERMISOS",
          permission: "10.7",
          icon: "visibility",
        },
      },
      // FALTAS
      {
        path: "/mis-faltas/lista",
        component: () => import("src/pages/Faltas/Listado.vue"),
        meta: {
          name: "VER MIS FALTAS",
          permission: "10.8",
          icon: "visibility",
        },
      },
      // CERTIFICADO LABORAL
      {
        path: "/mis-certificados/lista",
        component: () => import("src/pages/CertificadoLaboral/Listado.vue"),
        meta: {
          name: "MIS CERTIFICADOS LABORALES",
          permission: "10.9",
          icon: "visibility",
        },
      },
      {
        path: "/mi-historial/lista",
        component: () => import("src/pages/HistorialLaboral/Listado.vue"),
        meta: {
          name: "MI HISTORIAL LABORAL",
          permission: "10.10",
          icon: "visibility",
        },
      },
      // CAPACITACIONES
      {
        path: "/mis-capacitaciones/lista",
        component: () => import("src/pages/Capacitaciones/Listado.vue"),
        meta: {
          name: "VER MIS CAPACITACIONES",
          permission: "10.11",
          icon: "visibility",
        },
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
