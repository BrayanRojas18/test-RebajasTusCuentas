const CompaniesController = require("../controllers/CompaniesController");
const router = require("express").Router();
const { body, validationResult } = require("express-validator");
const {verifyToken} = require("../middleware/auth");

router.get("/list", verifyToken, async function (req, res) {
  await CompaniesController.List(req, res);
});
router.post("/get_one", verifyToken, async function (req, res) {
  await CompaniesController.GetOne(req, res);
});
router.post("/create", body("email").isEmail(), verifyToken, async function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res
      .status(400)
      .json({ error: "El correo ingresado no es un correo", status: 400 });
  }
  await CompaniesController.Create(req, res);
});
router.post("/update", body("email").isEmail(), verifyToken, async function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res
      .status(400)
      .json({ error: "El correo ingresado no es un correo", status: 400 });
  }
  await CompaniesController.Update(req, res);
});
router.post("/update-status", verifyToken, async function (req, res) {
  await CompaniesController.UpdateStatus(req, res);
});
router.post("/delete", verifyToken, async function (req, res) {
  await CompaniesController.Delete(req, res);
});

module.exports = router;
