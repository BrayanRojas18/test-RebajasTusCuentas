var router = require("express").Router();

router.use("/usuario", require("./routes-usuario"));
router.use("/companies", require("./routes-companies"));
router.use("/configuraciones", require("./routes-configuraciones"));
router.use("/prestamos", require("./routes-prestamos"));
router.use("/anticipos", require("./routes-anticipos"));
router.use("/permisos", require("./routes-permisos"));
router.use("/vacaciones", require("./routes-vacaciones"));
router.use("/faltas", require("./routes-faltas"));
router.use(
  "/certificados-laborales",
  require("./routes-certificados-laborales")
);
router.use("/historial-laboral", require("./routes-historial-laboral"));
router.use("/capacitaciones", require("./routes-capacitaciones"));
router.use("/roles", require("./routes-roles"));
router.use("/aprobaciones", require("./routes-aprobaciones"));
router.use("/departamentos", require("./routes-departamentos"));
router.use("/notificaciones", require("./routes-notificaciones"));
router.use("/rubros", require("./routes-rubros"));

module.exports = router;
