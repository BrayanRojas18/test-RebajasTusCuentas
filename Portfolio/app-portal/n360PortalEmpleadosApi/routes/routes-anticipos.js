const AnticiposController = require("../controllers/AnticiposController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// Solicitar
router.post("/solicitar", verifyToken, async function (req, res) {
  await AnticiposController.Solicitar(req, res);
});

// VALIDACIONES & CONSULTAS
router.post("/monto-maximo", verifyToken, async function (req, res) {
  await AnticiposController.MontoMaximo(req, res);
});
router.post("/solicitudes", verifyToken, async function (req, res) {
  await AnticiposController.Solicitudes(req, res);
});
router.post("/activos-pagados", verifyToken, async function (req, res) {
  await AnticiposController.Activos(req, res);
});
router.post("/historico", verifyToken, async function (req, res) {
  await AnticiposController.Historico(req, res);
});
router.post("/eliminar", verifyToken, async function (req, res) {
  await AnticiposController.Eliminar(req, res);
});
router.post("/get-anticipo", verifyToken, async function (req, res) {
  await AnticiposController.GetOne(req, res);
});
router.post("/mi-aprobador", verifyToken, async function (req, res) {
  await AnticiposController.MiAprobador(req, res);
});
router.post("/upload-file", verifyToken, async function (req, res) {
  await AnticiposController.UploadFile(req, res);
});
router.post("/remove-file", verifyToken, async function (req, res) {
  await AnticiposController.RemoveFile(req, res);
});
router.post("/motivos", verifyToken, async function (req, res) {
  await AnticiposController.Motivos(req, res);
});
router.post("/roles", verifyToken, async function (req, res) {
  await AnticiposController.Roles(req, res);
});
router.post("/configuracion", verifyToken, async function (req, res) {
  await AnticiposController.Config(req, res);
});
router.post("/beneficios", verifyToken, async function (req, res) {
  await AnticiposController.Beneficios(req, res);
});
router.post("/ultimo-rol", verifyToken, async function (req, res) {
  await AnticiposController.UltimoRol(req, res);
});

router.post("/set-fecha", verifyToken, async function (req, res) {
  await AnticiposController.SetFecha(req, res);
});

module.exports = router;
