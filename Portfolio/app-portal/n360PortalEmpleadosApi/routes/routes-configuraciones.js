const ConfiguracionesController = require("../controllers/ConfiguracionesController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

router.get("/configuracion-empresa", verifyToken, async function (req, res) {
  let conf = JSON.parse(req.query.config);

  const instance = new DB(req, res, conf);
  try {
    const val = await instance.get("configuracion/empresa");
    res.status(200).json({
      result: true,
      data: val,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send("Internal server error");
  }
});
router.get("/configuracion-prestamos", verifyToken, async function (req, res) {
  let conf = JSON.parse(req.query.config);

  const instance = new DB(req, res, conf);
  try {
    const val = await instance.get("configuracion/prestamos");
    res.status(200).json({
      result: true,
      data: val,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send("Internal server error");
  }
});
router.post("/menu", verifyToken, async function (req, res) {
  await ConfiguracionesController.menu(req, res);
});

module.exports = router;
