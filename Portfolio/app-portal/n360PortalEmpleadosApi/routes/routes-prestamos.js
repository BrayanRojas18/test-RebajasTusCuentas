const PrestamosController = require("../controllers/PrestamosController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// Solicitar
router.post("/solicitar", verifyToken, async function (req, res) {
  await PrestamosController.Solicitar(req, res);
});

// VALIDACIONES & CONSULTAS
router.post("/tipo-sueldo", verifyToken, async function (req, res) {
  await PrestamosController.TipoSueldo(req, res);
});
router.post("/configuracion-prestamos", verifyToken, async function (req, res) {
  await PrestamosController.ConfiguracionPrestamos(req, res);
});
router.post("/monto-maximo", verifyToken, async function (req, res) {
  await PrestamosController.MontoMaximo(req, res);
});
router.post("/mi-aprobador", verifyToken, async function (req, res) {
  await PrestamosController.MiAprobador(req, res);
});
router.post("/solicitudes", verifyToken, async function (req, res) {
  await PrestamosController.Solicitudes(req, res);
});
router.post("/activos-pagados", verifyToken, async function (req, res) {
  await PrestamosController.Activos(req, res);
});
router.post("/historico", verifyToken, async function (req, res) {
  await PrestamosController.Historico(req, res);
});
router.post("/eliminar", verifyToken, async function (req, res) {
  await PrestamosController.Eliminar(req, res);
});
router.post("/estado-pdf", verifyToken, async function (req, res) {
  await PrestamosController.EstadoCuentaPdf(req, res);
});
router.post("/get-prestamo", verifyToken, async function (req, res) {
  await PrestamosController.GetOne(req, res);
});
router.post("/upload-file", verifyToken, async function (req, res) {
  await PrestamosController.UploadFile(req, res);
});
router.post("/remove-file", verifyToken, async function (req, res) {
  await PrestamosController.RemoveFile(req, res);
});

module.exports = router;
