const DepartamentosController = require("../controllers/DepartamentosController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// CONSULTAS
router.post("/listado", verifyToken, async function (req, res) {
  await DepartamentosController.Listado(req, res);
});

module.exports = router;
