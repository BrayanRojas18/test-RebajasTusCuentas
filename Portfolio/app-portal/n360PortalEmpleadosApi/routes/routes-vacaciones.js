const VacacionesController = require("../controllers/VacacionesController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// Solicitar
router.post("/solicitar", verifyToken, async function (req, res) {
  await VacacionesController.Solicitar(req, res);
});

// VALIDACIONES & CONSULTAS
router.post("/get-vacaciones-list-total", verifyToken, async function (req, res) {
  await VacacionesController.getListTotal(req, res);
});
router.post("/get-vacaciones-list", verifyToken, async function (req, res) {
  await VacacionesController.getList(req, res);
});
router.post("/mi-aprobador", verifyToken, async function (req, res) {
  await VacacionesController.MiAprobador(req, res);
});
router.post("/solicitudes", verifyToken, async function (req, res) {
  await VacacionesController.Solicitudes(req, res);
});
router.post("/activos-pagados", verifyToken, async function (req, res) {
  await VacacionesController.Activos(req, res);
});
router.post("/eliminar", verifyToken, async function (req, res) {
  await VacacionesController.Eliminar(req, res);
});
router.post("/historico", verifyToken, async function (req, res) {
  await VacacionesController.Historico(req, res);
});
router.post("/get-vacacion", verifyToken, async function (req, res) {
  await VacacionesController.GetOne(req, res);
});
router.post("/upload-file", verifyToken, async function (req, res) {
  await VacacionesController.UploadFile(req, res);
});
router.post("/remove-file", verifyToken, async function (req, res) {
  await VacacionesController.RemoveFile(req, res);
});

module.exports = router;
