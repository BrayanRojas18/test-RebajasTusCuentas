const CertificadosLaborales = require("../controllers/CertificadosLaborales");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// CONSULTAS
router.post("/listado", verifyToken, async function (req, res) {
  await CertificadosLaborales.Listado(req, res);
});
router.post("/pdf", verifyToken, async function (req, res) {
  await CertificadosLaborales.Pdf(req, res);
});

module.exports = router;
