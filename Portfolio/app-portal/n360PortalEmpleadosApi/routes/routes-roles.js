const RolesController = require("../controllers/RolesController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// CONSULTAS
router.post("/listado", verifyToken, async function (req, res) {
  await RolesController.Listado(req, res);
});
router.post("/tipos-roles", verifyToken, async function (req, res) {
  await RolesController.TiposRoles(req, res);
});
router.post("/recibir", verifyToken, async function (req, res) {
  await RolesController.Recibir(req, res);
});
router.post("/visualizar", verifyToken, async function (req, res) {
  await RolesController.Visualizar(req, res);
});
router.post("/role-index", verifyToken, async function (req, res) {
  await RolesController.RoleIndex(req, res);
});
router.post("/roles", verifyToken, async function (req, res) {
  await RolesController.Roles(req, res);
});

module.exports = router;
