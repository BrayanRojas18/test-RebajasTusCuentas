const { Storage } = require("@google-cloud/storage");

let projectId = "";
let keyFilename = "";
const storage = new Storage({
  projectId,
  keyFilename,
});
const bucket = storage.bucket("n360-data-integracion-portal");

module.exports = {
  upload: async ({ filePath, destFileName }) => {
    return new Promise((resolve, reject) => {
      const options = {
        destination: destFileName,
        preconditionOpts: { ifGenerationMatch: 0 },
      };

      bucket
        .upload(filePath, options)
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  remove: async ({ fileName }) => {
    return new Promise((resolve, reject) => {
      const options = {
        ifGenerationMatch: 0,
      };

      bucket
        .file(fileName)
        .delete()
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};
