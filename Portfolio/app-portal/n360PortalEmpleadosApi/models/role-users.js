const mongoose = require("mongoose");
const { Schema } = mongoose;
const moment = require("moment");

const roleUsersSchema = new Schema({
  id: {
    type: String,
    required: true,
  },
  permissions: {
    type: Array,
    required: true,
  },
  __company__: {
    type: mongoose.ObjectId,
    required: true,
  },
  created_at: {
    type: Date,
    default: moment(),
  },
});

module.exports = mongoose.model("role-users", roleUsersSchema);
