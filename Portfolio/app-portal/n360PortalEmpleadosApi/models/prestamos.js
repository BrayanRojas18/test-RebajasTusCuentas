const mongoose = require("mongoose");
const { Schema } = mongoose;
const moment = require("moment");

const prestamosSchema = new Schema({
  employee: {
    type: String,
    required: true,
  },
  motivo: {
    type: String,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },
  date: {
    type: String,
    required: true,
  },
  dues: {
    type: String,
    required: true,
  },
  cuotas: {
    type: Array,
    required: true,
  },
  tipo_prestamo: {
    type: String,
  },
  ingreso: {
    type: Boolean,
  },
  estatus: {
    type: String,
    required: true,
    default: "p",
  },
  aprobador: {
    type: String,
  },
  file: {
    type: String,
  },
  site: {
    type: String,
    default: "portal",
  },
  prestamo_id: {
    type: String,
  },
  __company__: {
    type: mongoose.ObjectId,
  },
  created_at: {
    type: Date,
    default: moment(),
  },
});

module.exports = mongoose.model("prestamos", prestamosSchema);
