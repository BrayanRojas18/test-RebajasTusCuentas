const mongoose = require("mongoose");
const { Schema } = mongoose;
const moment = require("moment");

const permisosSchema = new Schema({
  employee: {
    type: String,
    required: true,
  },
  solicitud: {
    type: String,
    required: true,
  },
  motivo: {
    type: String,
  },
  inicio: {
    type: String,
    required: true,
  },
  fin: {
    type: String,
    required: true,
  },
  dias: {
    type: Number,
  },
  estatus: {
    type: String,
    required: true,
    default: "p",
  },
  aprobador: {
    type: String,
  },
  file: {
    type: String,
  },
  site: {
    type: String,
    default: "portal",
  },
  permiso_id: {
    type: String,
  },
  __company__: {
    type: mongoose.ObjectId,
  },
  created_at: {
    type: Date,
    default: moment(),
  },
});

module.exports = mongoose.model("permisos", permisosSchema);
