const mongoose = require("mongoose");
const { Request, Response } = require("express");
const RoleUsers = require("../models/role-users.js");

const platEmpleado = async () => {
  return [
    {
      _id: "10.1",
      id: "my-personal-file",
      label: "Mi ficha personal",
      level: 1,
      icon: "assignment",
      url: "/mi-ficha-personal",
      isCollapsible: true,
      nopermission: true,
      active: true,
      colaborador: true,
      children: [
        {
          _id: "10.1.1",
          id: "my-personal-file-docs",
          label: "Permitir subir documentos",
          level: 1,
          icon: "assignment",
          isCollapsible: false,
          nopermission: true,
          active: true,
          colaborador: true,
        },
      ],
    },
    {
      _id: "10.2",
      id: "my-department",
      label: "Mi Departamento",
      level: 1,
      icon: "fa fa-users",
      url: "/mi-departamento",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.3",
      id: "my-roles",
      label: "Mis roles",
      level: 1,
      icon: "payment",
      url: "/mis-roles",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.4",
      id: "my-advances",
      label: "Mis anticipos",
      level: 1,
      icon: "payment",
      url: "/mis-anticipos",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.5",
      id: "my-payday-loans",
      label: "Mis préstamos",
      level: 1,
      icon: "payment",
      url: "/mis-prestamos",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.6",
      id: "my-vacations",
      label: "Mis Vacaciones",
      level: 1,
      icon: "waves",
      url: "/mis-vacaciones",
      isCollapsible: true,
      nopermission: true,
      active: true,
      slug: "vacaciones",
      colaborador: true,
      children: [
        {
          _id: "10.6.1",
          id: "my-vacations-nomal",
          label: "Vacaciones Normales",
          level: 1,
          icon: "waves",
          isCollapsible: false,
          nopermission: true,
          active: true,
          colaborador: true,
        },
        {
          _id: "10.6.2",
          id: "my-vacations-additional",
          label: "Vacaciones Adicionales",
          level: 1,
          icon: "add",
          isCollapsible: true,
          nopermission: true,
          active: true,
          colaborador: true,
          children: [
            {
              _id: "10.6.2.1",
              id: "my-vacations-additional-normal",
              label: "Normal",
              level: 1,
              isCollapsible: false,
              nopermission: true,
              active: true,
              colaborador: true,
            },
            {
              _id: "10.6.2.2",
              id: "my-vacations-additional-pay_discount",
              label: "Pagados y Descontados",
              level: 1,
              isCollapsible: false,
              nopermission: true,
              active: true,
              colaborador: true,
            },
            {
              _id: "10.6.2.3",
              id: "my-vacations-additional-days",
              label: "Descontar Días",
              level: 1,
              isCollapsible: false,
              nopermission: true,
              active: true,
              colaborador: true,
            },
            {
              _id: "10.6.2.4",
              id: "my-vacations-additional-pay",
              label: "Descontar Pago",
              level: 1,
              isCollapsible: false,
              nopermission: true,
              active: true,
              colaborador: true,
            },
          ],
        },
      ],
    },
    {
      _id: "10.7",
      id: "my-permission",
      label: "Mis Permisos",
      level: 1,
      icon: "accessibility",
      url: "/mi-permisos",
      isCollapsible: true,
      nopermission: true,
      active: true,
      colaborador: true,
      children: [
        {
          _id: "10.7.1",
          id: "my-permission-medicals",
          label: "Permisos Médicos",
          level: 1,
          icon: "fas fa-file-medical",
          isCollapsible: false,
          nopermission: true,
          active: true,
          colaborador: true,
        },
        {
          _id: "10.7.2",
          id: "my-permission-paternity",
          label: "Permisos Paternidad",
          level: 1,
          icon: "fas fa-male",
          isCollapsible: false,
          nopermission: true,
          active: true,
          colaborador: true,
        },
        {
          _id: "10.7.3",
          id: "my-permission-maternity",
          label: "Permisos Maternidad",
          level: 1,
          icon: "fas fa-female",
          isCollapsible: false,
          nopermission: true,
          active: true,
          colaborador: true,
        },
        {
          _id: "10.7.4",
          id: "my-permission-others",
          label: "Permisos Otros",
          level: 1,
          icon: "fas fa-file",
          isCollapsible: false,
          nopermission: true,
          active: true,
          colaborador: true,
        },
      ],
    },
    {
      _id: "10.8",
      id: "my-faults",
      label: "Mis Faltas",
      level: 1,
      icon: "time_to_leave",
      url: "/mis-faltas",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.9",
      id: "my-work-certificates",
      label: "Mis Certificados de Trabajo",
      level: 1,
      icon: "work",
      url: "/mis-certificados-trabajo",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.10",
      id: "my-history-laboral",
      label: "Mi Historia Laboral",
      level: 1,
      icon: "work",
      url: "/mi-historia",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.11",
      id: "my-mualtas",
      label: "Mis Multas",
      level: 1,
      icon: "work",
      url: "/mis-multas",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.12",
      id: "my-llamados-de-atencion",
      label: "Mis Llamados de Atención",
      level: 1,
      icon: "work",
      url: "/mis-llamados-de-atencion",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.13",
      id: "my-capacitaciones",
      label: "Mis Capacitaciones",
      level: 1,
      icon: "work",
      url: "/mis-capacitacion",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.14",
      id: "my-asistencias",
      label: "Mis Asistencias",
      level: 1,
      icon: "work",
      url: "/mis-asistencias",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.16",
      id: "my-boletin",
      label: "Boletin de Novedades",
      level: 1,
      icon: "work",
      url: "/mi-boletin",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.17",
      id: "my-otros-portales",
      label: "Otros Portales",
      level: 1,
      icon: "work",
      url: "/mis-otros-portales",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
    {
      _id: "10.18",
      id: "my-gastos-personales",
      label: "Gastos Personales",
      level: 1,
      icon: "work",
      url: "/mis-gastos-personales",
      isCollapsible: false,
      nopermission: true,
      active: true,
      colaborador: true,
    },
  ];
};
const construir_menu = (rol) => {
  return new Promise(async (resolve, reject) => {
    let menu = {};
    if (rol === "colaborador") {
      menu.menu = await platEmpleado();
      resolve(menu);
    }
  });
};
const buscar2 = async (mn) => {
  for (let x in mn) {
    if (!mn[x].rol) {
      mn.splice(x, 1);
      continue;
    }
    if (mn[x].hasOwnProperty("children") && mn[x].rol) buscar2(mn[x].children);
  }
  return mn;
};
const buscar3 = async (mn, rolP) => {
  let colaborador = false;
  for (let x in mn) {
    let exp = new RegExp("(^" + mn[x]._id + ")", "gm");
    mn[x]["rol"] = rolP.some((val) => val === mn[x]._id || exp.test(val));
    if (mn[x].hasOwnProperty("colaborador")) mn[x].colaborador = colaborador;
    if (mn[x].hasOwnProperty("children")) buscar3(mn[x].children, rolP);
  }
  return mn;
};
module.exports = {
  menu: async (request = Request, response = Response) => {
    try {
      const data = request.body;

      let resultado = {};
      let menuP = await construir_menu(data.rol);
      let menu = menuP.menu;
      let rolP = [];

      const rol_act = await RoleUsers.findOne({
        id: data.rol,
      });
      if (rol_act) rolP.push(...rol_act.permissions);

      delete menu[1]["children"];
      rolP = [...new Set(rolP)];
      await rolP.sort();
      menu = await buscar3(menu, rolP);
      menu = await buscar2(menu);
      resultado = { role: rolP, menu };
      return response.status(200).json({
        result: true,
        data: resultado,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
