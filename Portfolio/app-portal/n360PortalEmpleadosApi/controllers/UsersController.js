const mongoose = require("mongoose");
const { api } = require("../db/api.js");
const bcrypt = require("bcrypt");
const { Request, Response } = require("express");
const jwt = require("jsonwebtoken");
const { key } = require("../env.js");
const Usuario = require("../models/usuarios.js");
const Companies = require("../models/companies.js");
const moment = require("moment");

const is_aprobador = async (url, company, empleado, token) => {
  const departamentos = await api("departamentos/departamentos", url, "GET", {
    conditions: {
      __company__: company,
    },
  }, token);
  if (departamentos.data.length) {
    for (let i in departamentos.data) {
      if (departamentos.data[i].hasOwnProperty("approve_prestamos")) {
        if (departamentos.data[i].approve_prestamos === empleado) return true;
      }
      if (departamentos.data[i].hasOwnProperty("approve_permisos")) {
        if (departamentos.data[i].approve_permisos === empleado) return true;
      }
      if (departamentos.data[i].hasOwnProperty("approve_anticipos")) {
        if (departamentos.data[i].approve_anticipos === empleado) return true;
      }
      if (departamentos.data[i].hasOwnProperty("approve_vacaciones")) {
        if (departamentos.data[i].approve_vacaciones === empleado) return true;
      }
    }
  }
  return false;
};
const get_route = async (role, esAprobador) => {
  let route = {
    main: "/home",
  };
  if (role === "colaborador") {
    route.main = "/home";
  }
  if (esAprobador) {
    route.main = "/pendientes-empleados";
  }
  if (role === "super-admin") {
    route.main = "/panel";
  }
  return route;
};

module.exports = {
  signUp: async (request = Request, response = Response) => {
    try {
      const data = request.body;

      // BUSCAR Y VALIDAR SI EL USUARIO EXISTE
      const usuario = await Usuario.findOne({
        email: data.email,
      });
      if (usuario)
        return response.status(403).json({
          error: "El correo electrónico ingresado ya se encuentra registrado. ",
          status: 403,
        });

      // BUSCAR EMPRESA Y EMPLEADO
      const companies = await Companies.find({ active: true }).exec();
      let user_exist = false;
      let empleado_id;
      let company;
      if (companies.length) {
        for (let i in companies) {
          let find_user = await api(
            "usuarios/usuarios",
            companies[i].apiUrl,
            "GET",
            {
              conditions: {
                identity_card: data.identity_card,
              },
            }, 
            request.headers["access-token"]
          );
          find_user = find_user.data.length ? find_user.data[0] : null;
          if (find_user) {
            const contrato = await api(
              "usuarios/contratos",
              companies[i].apiUrl,
              "GET",
              {
                conditions: {
                  empleado: find_user._id,
                  $or: [
                    { liquidado: { $exists: false } },
                    { liquidado: false },
                  ],
                },
              }, 
              request.headers["access-token"]
            );
            if (contrato.data.length) {
              user_exist = true;
              empleado_id = find_user._id;
              company = companies[i]._id;
            }
          }
        }
      }

      // GUARDAR EMPELADO SI ES ENCONTRADO EN ALGUNA EMPRESA SEGUN SU API
      if (user_exist) {
        // VALIDAR SI EL USUARIO YA ESTA REGISTRADO SEGUN CEDULA
        const regsitrado = await Usuario.findOne({
          empleado_id: mongoose.Types.ObjectId(empleado_id),
        });
        if (regsitrado)
          return response.status(403).json({
            error:
              "Según el número de cedula, el usuario ya se encuentra registrado. ",
            status: 403,
          });

        const saltRounds = 10;
        const encrypted_password = await bcrypt.hashSync(
          data.password,
          saltRounds
        );
        await Usuario.create({
          email: data.email,
          password: encrypted_password,
          empleado_id: mongoose.Types.ObjectId(empleado_id),
          active: true,
          role: "colaborador",
          __company__: company,
        });
        return response.status(200).json({ result: true, data: true });
      } else {
        return response.status(403).json({
          error:
            "El número de cedula ingresado no coincide con ningún usuario creado en alguna empresa o no posee algún contrato activo. ",
          status: 403,
        });
      }
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  login: async (request = Request, response = Response) => {
    try {
      const data = request.body;

      // OBTENER DATA DEL USUARIO

      let usuario = await Usuario.findOne({
        email: data.email,
      });
      if (usuario) {
        // VALIDAR USUARIO ACTIVO
        if (usuario.role == "colaborador" && !usuario.active) {
          return response.status(403).json({
            error: "Usuario inactivo",
            status: 403,
          });
        }

        // BUSCAR EMPRESA
        const company_user = usuario.__company__;
        const company = await Companies.findById(company_user).exec();
        if (!company && usuario.role !== "super-admin") {
          return response.status(403).json({
            error:
              "No se ha podido encontrar una empresa registrada según los dato del usuario",
            status: 403,
          });
        }

        // BUSCAR DATA DEL USUARIO EN API DE LA EMPRESA
        if (usuario.role !== "super-admin") {
          const find_user = await api(
            "usuarios/usuarios",
            company.apiUrl,
            "GET",
            {
              conditions: {
                _id: usuario.empleado_id,
              },
            }, request.headers["access-token"]
          );
          if (find_user.data.length) {
            // VALIDAR SI TIENE CONTRATO ACTIVO SI ES COLABORADOR
            if (usuario.role == "colaborador") {
              const contrato = await api(
                "usuarios/contratos",
                company.apiUrl,
                "GET",
                {
                  conditions: {
                    empleado: usuario.empleado_id,
                    $or: [
                      { liquidado: false },
                      { liquidado: { $exists: false } },
                    ],
                  },
                }, request.headers["access-token"]
              );
              if (!contrato.data.length) {
                return response.status(403).json({
                  error: "El usuario no tiene un contrato activo",
                  status: 403,
                });
              }
            }
            // VERIFICAR CONTRASEÑA
            const compare_password = await bcrypt.compareSync(
              data.password,
              usuario.password
            );
            if (compare_password) {
              // BUSCAR SI ES APROBADOR
              const aprobador = await is_aprobador(
                company.apiUrl,
                find_user.data[0].__company__,
                find_user.data[0]._id,
                request.headers["access-token"]
              );

              // BUSCAR RUTA
              const route = await get_route(usuario.role, aprobador);

              // GENERAR TOKEN
              const payload = {
                user_id: usuario._id,
                email: data.email,
              };
              const expirationTime = "5h";
              const token = jwt.sign(payload, key, {
                expiresIn: expirationTime,
              });

              // LOGIN
              return response.status(200).json({
                result: true,
                data: {
                  usuario: { ...usuario._doc, data: find_user.data[0] },
                  token,
                  empresa: company,
                  rol: usuario.role,
                  route,
                  isUsuarioAprobadorDpto: aprobador,
                },
              });
            } else {
              return response.status(403).json({
                error: "Contraseña Inválida",
                status: 403,
              });
            }
          } else {
            return response.status(403).json({
              error:
                "Usuario registrado, pero no se ha encontrado data en su empresa original",
              status: 403,
            });
          }
        } else {
          // VERIFICAR CONTRASEÑA
          const compare_password = await bcrypt.compareSync(
            data.password,
            usuario.password
          );
          if (compare_password) {
            // BUSCAR RUTA
            const route = await get_route(usuario.role, false);

            // GENERAR TOKEN
            const payload = {
              user_id: usuario._id,
              email: data.email,
            };
            const expirationTime = "5h";
            const token = jwt.sign(payload, key, {
              expiresIn: expirationTime,
            });

            // LOGIN
            return response.status(200).json({
              result: true,
              data: {
                usuario,
                token,
                empresa: {},
                rol: usuario.role,
                route,
                isUsuarioAprobadorDpto: false,
              },
            });
          } else {
            return response.status(403).json({
              error: "Contraseña Inválida",
              status: 403,
            });
          }
        }
      } else {
        return response.status(403).json({
          error:
            "Usuario no registrado. Por favor realice el proceso de registro por primera vez.",
          status: 403,
        });
      }
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  home: async (request = Request, response = Response) => {
    try {
      const { user, apiUrl, company } = request.body;

      let data = {};

      // BUSCAR ROLES GENERADOS Y APROBADOS
      let generate_role = await api("roles/roles", apiUrl, "GET", {
        conditions: {
          aprobado: true,
          __company__: company,
        },
      }, request.headers["access-token"]);
      generate_role = generate_role.data.length ? generate_role.data : [];

      // NOMBRE Y APELLIDO DEL EMPLEADO
      let usuarios = await api("usuarios/usuarios", apiUrl, "GET", {
        conditions: {
          __company__: company,
        },
      }, request.headers["access-token"]);
      usuarios = usuarios.data.length ? usuarios.data : [];
      const usuario = usuarios.find((v) => v._id == user.empleado_id);
      if (usuario) {
        data["full_name"] = usuario.name + " " + usuario.last_name;
        data["email"] = usuario.email_personal;
      }

      // Dias de contrato actual
      let contrato = await api("usuarios/contratos", apiUrl, "GET", {
        conditions: {
          empleado: user.empleado_id,
          $or: [{ liquidado: { $exists: false } }, { liquidado: false }],
        },
      }, request.headers["access-token"]);
      contrato = contrato.data.length ? contrato.data[0] : {};
      data["contrato_days"] = moment().diff(
        moment(contrato.entry_date),
        "days"
      );

      // Departamento y Jefe
      let dpt = await api("departamentos/departamentos", apiUrl, "GET", {
        conditions: {
          _id: contrato.departamento,
        },
      }, request.headers["access-token"]);
      dpt = dpt.data.length ? dpt.data[0] : {};
      data["departamento"] = {};
      if (dpt) {
        if (dpt.jefe_dpt) {
          const jefe_dpt = usuarios.find((v) => v._id == dpt.jefe_dpt);
          data["departamento"].jefe = jefe_dpt
            ? jefe_dpt.name + " " + jefe_dpt.last_name
            : "Sin Jefe";
        } else {
          data["departamento"].jefe = "Sin Jefe"
        }
        data["departamento"].name = dpt.name;
      }

      // Dias de vacaciones disponibles
      let vacaciones = await api(
        "vacaciones/get-vacaciones-list-total",
        apiUrl,
        "POST",
        {
          user: user.empleado_id,
          config: { company: user.data.__company__ },
        }, request.headers["access-token"]
      );
      vacaciones = vacaciones.data ? vacaciones.data : {};
      if (vacaciones.disponibles.length) {
        const disponibles = vacaciones.disponibles;
        let dias = 0;
        let dias_a = 0;
        disponibles.forEach((v) => {
          dias += Number(v.dias);
          dias_a += Number(v.dias_adicionales);
        });
        data["dias_vacaciones"] = dias;
        data["dias_vacaciones_a"] = dias_a;
        data["dias_vacaciones_tot"] = dias + dias_a;
      } else {
        data["dias_vacaciones"] = 0;
        data["dias_vacaciones_a"] = 0;
        data["dias_vacaciones_tot"] = 0;
      }

      // Prestamos Pendientes
      let prestamos = await api("prestamos/listado", apiUrl, "GET", {
        conditions: {
          employee: user.empleado_id,
        },
      }, request.headers["access-token"]);
      prestamos = prestamos.data.length ? prestamos.data : [];
      if (prestamos.length) {
        let num_prestamos = 0;
        for (let i in prestamos) {
          const last = prestamos[i].cuotas[prestamos[i].cuotas.length - 1];
          const find = generate_role.find(
            (v) => v.year == last.year && v.month == last.month
          );
          if (!find) num_prestamos += 1;
        }
        data["prestamos_pendientes"] = num_prestamos;
      } else {
        data["prestamos_pendientes"] = 0;
      }

      // Dinero neto del año en curso
      let role_index = await api("roles/roles-index", apiUrl, "GET", {
        conditions: {
          $or: generate_role.map((v) => ({
            rol: v._id,
          })),
          anho_rol: moment().year(),
          contrato_empleado: user.empleado_id,
        },
      }, request.headers["access-token"]);
      role_index = role_index.data.length ? role_index.data : [];
      if (role_index.length) {
        let neto = 0;
        role_index.forEach((r) => {
          neto += Number(r.net);
        });
        data["neto"] = Number.parseFloat(neto).toFixed(2);
      } else {
        data["neto"] = 0.0;
      }

      // Llamados de atencion
      let llamados = await api("llamados/listado", apiUrl, "GET", {
        conditions: {
          employee: user.empleado_id,
        },
      }, request.headers["access-token"]);
      llamados = llamados.data.length ? llamados.data : [];
      if (llamados.length) {
        let total = 0;
        llamados.forEach((v) => {
          if (moment(v.date).year() === moment().year()) total += 1;
        });
        data["llamados_atencion"] = total;
      } else {
        data["llamados_atencion"] = 0;
      }

      // Dias de falata en el año
      let faltas = await api("faltas/listado", apiUrl, "GET", {
        conditions: {
          employee: user.empleado_id,
          termtype: "d",
        },
      }, request.headers["access-token"]);
      faltas = faltas.data.length ? faltas.data : [];
      if (faltas.length) {
        let total = 0;
        faltas.forEach((v) => {
          if (moment(v.date).year() === moment().year()) total += v.term;
        });
        data["dias_faltas"] = total;
      } else {
        data["dias_faltas"] = 0;
      }

      // Dias de Permiso en el año
      let permisos = await api("permisos/listado", apiUrl, "GET", {
        conditions: {
          empleado: user.empleado_id,
        },
      }, request.headers["access-token"]);
      permisos = permisos.data.length ? permisos.data : [];
      if (permisos.length) {
        let total = 0;
        permisos.forEach((v) => {
          if (moment(v.inicio).year() === moment().year()) total += v.dias;
        });
        data["dias_permisos"] = total;
      } else {
        data["dias_permisos"] = 0;
      }

      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
