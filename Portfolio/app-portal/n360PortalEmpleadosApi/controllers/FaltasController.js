const { Request, Response } = require("express");
const { api } = require("../db/api");
const moment = require("moment");

module.exports = {
  // --------------------------------//
  Listado: async (request = Request, response = Response) => {
    try {
      const { user, url, type } = request.body;
      //BUSCAR FALTAS GENERADAS EN EL API EXTERNO
      let conditions = {
        employee: user.data._id,
      };
      if (type !== "all") conditions["type"] = type;
      let solicitudes = await api("faltas/listado", url, "GET", {
        conditions,
      }, request.headers["access-token"]);
      const data = solicitudes.data.sort(
        (a, b) =>
          moment(b.date, "DD-MM").unix() - moment(a.date, "DD-MM").unix()
      );
      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  MotivosFaltas: async (request = Request, response = Response) => {
    try {
      const { user, url } = request.body;

      let motivos = await api("faltas/motivos-faltas", url, "GET", {
        conditions: { __company__: user.data.__company__ },
      }, request.headers["access-token"]);
      return response.status(200).json({
        result: true,
        data: motivos.data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
