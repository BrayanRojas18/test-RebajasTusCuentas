const { api } = require("../db/api.js");
const { socket } = require("../index");
const Usuarios = require("../models/usuarios.js");
const Notificaciones = require("../models/notificaciones.js");
const moment = require("moment");
const fs = require("fs");
const mongoose = require("mongoose");
const Anticipos = require("../models/anticipos.js");
const Prestamos = require("../models/prestamos.js");
const Permisos = require("../models/permisos.js");
const Vacaciones = require("../models/vacaciones.js");

const find_apro_dpt = async (url, user, tipo, token) => {
  let contrato = await api("usuarios/contratos", url, "GET", {
    conditions: {
      empleado: user,
      $or: [{ liquidado: { $exists: false } }, { liquidado: false }],
    },
  });
  contrato = contrato.data.length ? contrato.data[0] : null;
  if (contrato) {
    let dpt = await api("departamentos/departamentos", url, "GET", {
      conditions: {
        _id: contrato.departamento,
      },
    }, token);

    let usuario = await api("usuarios/usuarios", url, "GET", {
      conditions: {
        _id: contrato.empleado,
      },
    });
    usuario = usuario.data.length ? usuario.data[0] : null;

    let type;
    if (tipo == "anticipo") type = "approve_anticipos";
    if (tipo == "prestamo") type = "approve_prestamos";
    if (tipo == "vacaciones") type = "approve_vacaciones";
    if (tipo == "permiso") type = "approve_permisos";

    return {
      apro: dpt.data.length && dpt.data[0][type] ? dpt.data[0][type] : null,
      apro_label: usuario ? usuario.name + " " + usuario.last_name : null,
      dpt: dpt.data.length ? dpt.data[0].name : null,
    };
  }
  return null;
};
const replace_vars = async (params) => {
  var value = params.content;

  if (value === undefined) value = "";

  value = value.replace(/\[([a-z].*?)\]/gi, function (val) {
    var variable = val.replace(/(\[|\])/g, "");
    if (
      params.variables[variable] &&
      moment(params.variables[variable]).isValid() &&
      String(params.variables[variable]).split("-").length >= 3
    )
      params.variables[variable] = moment(
        params.variables[variable],
        "YYYY-MM-DD"
      ).format("YYYY-MM-DD");
    return params.variables[variable];
  });

  value =
    value !== "" &&
    (Number(value) || Number(value) === 0) &&
    value.trim().split("")[0] !== "0"
      ? Number(value)
      : value;

  if (value === undefined || value === "undefined") value = "";

  return value;
};
module.exports = {
  MailAprobar: async (user, company, parent_company, url, tipo_solicitud) => {
    try {
      // BUSCAR APROBADOR DE DEPARTAMENTO
      const apro_dpt = await find_apro_dpt(url, user.data._id, tipo_solicitud, request.headers["access-token"]);

      await api("notificaciones/eventos", url, "POST", {
        config: { company, parent_company },
        ruta: "notificaciones/aprobadores-dpt",
        form: {
          fecha: moment().format("YYYY-MM-DD"),
          tipo_solicitud: tipo_solicitud,
          empleado: user.data._id,
          aprobador: [apro_dpt.apro],
          dpt: apro_dpt.dpt,
        },
      }, request.headers["access-token"]);

      return true;
    } catch (error) {
      return false;
    }
  },
  MailRechazar: async (
    user,
    company,
    parent_company,
    url,
    data,
    tipo_solicitud
  ) => {
    try {
      // BUSCAR APROBADOR DE DEPARTAMENTO
      const apro_dpt = await find_apro_dpt(url, user.data._id, tipo_solicitud, request.headers["access-token"]);

      // BUSCAR EMPLEADO
      const emp = await Usuarios.findById(data.employee).exec();

      const date = tipo_solicitud == "vacaciones" ? data.start : data.date;
      await api("notificaciones/eventos", url, "POST", {
        config: { company, parent_company },
        ruta: "notificaciones/rechazadas-dpt",
        form: {
          fecha: moment(date).format("YYYY-MM-DD"),
          tipo_solicitud: tipo_solicitud,
          empleado: emp.empleado_id,
          aprobador: [user.data._id],
          dpt: apro_dpt.dpt,
        },
      }, request.headers["access-token"]);

      return true;
    } catch (error) {
      return false;
    }
  },
  Notify: async (user, company, parent_company, url, data, action) => {
    try {
      // BUSCAR APROBADOR DE DEPARTAMENTO
      let tipo_solicitud;
      if (data.type_solicitud == "anticipos") tipo_solicitud = "anticipo";
      if (data.type_solicitud == "prestamos") tipo_solicitud = "prestamo";
      if (data.type_solicitud == "vacaciones") tipo_solicitud = "vacaciones";
      if (data.type_solicitud == "permisos") tipo_solicitud = "permiso";
      const apro_dpt = await find_apro_dpt(url, user.data._id, tipo_solicitud, request.headers["access-token"]);
      let type = action == "aprobar" ? "aprobados" : "rechazados";

      const evento = await api("notificaciones/evento-portal", url, "POST", {
        config: { company, parent_company },
        form: {
          type: type,
        },
      });

      if (evento.data) {
        // BUSCAR EMPLEADO
        const emp = await Usuarios.findById(data.employee).exec();

        // VALIDAR SI EL EMPLEADO QUIERE RECIBIR NOTIFICACITONS
        if (!emp.notifications) return true;

        let usuario = await api("usuarios/usuarios", url, "GET", {
          conditions: {
            _id: emp.empleado_id,
          },
        }, request.headers["access-token"]);
        usuario = usuario.data.length ? usuario.data[0] : {};

        // REEMPLAZAR VARIABLES
        let content = await replace_vars({
          variables: {
            aprobador: apro_dpt.apro_label,
            dpt: apro_dpt.dpt,
            tipo_solicitud: data.type_solicitud,
            first_name: usuario.name,
            ...usuario,
          },
          content: evento.data.contenido,
        });

        let title = "";
        if (action == "aprobar") title = "Se ha aprobado una solicitud";
        if (action == "rechazar") title = "Se ha rechazado una solicitud";
        let form = {
          title: title,
          content: content,
          employee: data.employee,
          solicitud: data._id,
          open: false,
          action: action,
          type: data.type_solicitud,
          site: "portal",
          __company__: mongoose.Types.ObjectId(user.__company__),
        };
        await Notificaciones.create(form);
      }

      return true;
    } catch (error) {
      return false;
    }
  },
  Listado: async (request = Request, response = Response) => {
    try {
      const { user, url } = request.body;

      // LISTADO DE NOTIFICACIONES
      let notificaciones = await Notificaciones.find({
        employee: user._id,
      });

      //BUSCAR NOTIFICAIONES EN EL API EXTERNO
      const listado = await api("notificaciones/listado", url, "POST", {
        config: { company: user.data.__company__ },
        data: { user: user.empleado_id, notificaciones: notificaciones },
      }, request.headers["access-token"]);

      const data = listado.data
        .map((v) => {
          return {
            ...v,
            created_at: new Date(v.created_at),
          };
        })
        .sort((a, b) => b.created_at - a.created_at);
      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Open: async (request = Request, response = Response) => {
    try {
      const { data, url, company } = request.body;

      if (data.site == "portal") {
        await Notificaciones.updateOne({ _id: data._id }, { open: true });
      } else {
        await api("notificaciones/open", url, "POST", {
          config: { company },
          collection: `notificaciones/${data._id}`,
        }, request.headers["access-token"]);
      }

      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  GetDoc: async (request = Request, response = Response) => {
    try {
      const { user, company, url, data } = request.body;

      let form;

      // PORTAL
      if (data.site == "portal") {
        if (data.type == "anticipos")
          form = await Anticipos.findById(data.solicitud).exec();
        if (data.type == "prestamos")
          form = await Prestamos.findById(data.solicitud).exec();
        if (data.type == "permisos")
          form = await Permisos.findById(data.solicitud).exec();
        if (data.type == "vacaciones")
          form = await Vacaciones.findById(data.solicitud).exec();
      }
      if (data.site == "th") {
        let collection = `${data.collection}/${data.solicitud}`;

        form = await api("notificaciones/get-doc", url, "POST", {
          config: { company },
          collection: collection,
        }, request.headers["access-token"]);
        if (form.data) form = form.data;
      }

      return response.status(200).json({
        result: true,
        data: form,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Configuracion: async (request = Request, response = Response) => {
    try {
      const { user, value } = request.body;

      await Usuarios.updateOne({ _id: user._id }, { notifications: value });

      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  GetConfig: async (request = Request, response = Response) => {
    try {
      const { user } = request.body;

      const emp = await Usuarios.findById(user._id).exec();
      let form = emp;

      return response.status(200).json({
        result: true,
        data: form,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
