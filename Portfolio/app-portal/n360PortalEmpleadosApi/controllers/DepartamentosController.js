const { Request, Response } = require("express");
const { api } = require("../db/api");
const moment = require("moment");

const find_dpts = async (url, company, token) => {
  const dpts = await api("departamentos/departamentos", url, "GET", {
    conditions: {
      __company__: company,
    },
  }, token);
  return dpts.data;
};
const find_users = async (url, company) => {
  const usuarios = await api("usuarios/usuarios", url, "GET", {
    conditions: {
      __company__: company,
    },
  });
  return usuarios.data;
};
const find_contratos = async (url, company) => {
  const contratos = await api("usuarios/contratos", url, "GET", {
    conditions: {
      __company__: company,
      $or: [{ liquidado: { $exists: false } }, { liquidado: false }],
    },
  });
  return contratos.data;
};

module.exports = {
  Listado: async (request = Request, response = Response) => {
    try {
      const { user, company, parent_company, url } = request.body;

      // BUSCAR TODOS LOS USUARIOS
      const usuarios = await find_users(url, company);

      // BUSCAR TODOS LOS CONTRATOS
      let contratos = await find_contratos(url, company);
      contratos = contratos.map((v) => {
        const find = usuarios.find((f) => f._id == v.empleado);
        if (find) return { ...v, ...find };
      });

      // BUSCAR TODOS LOS DPTS DE LA EMPRESA
      let dpts = await find_dpts(url, company, request.headers["access-token"]);

      // MAPEAR DPTS
      dpts = dpts.map((v) => {
        let emps = contratos.filter((f) => f && f.departamento == v._id);
        const jefe_dpt = usuarios.find((f) => f._id == v.jefe_dpt);
        const approve_anticipos = usuarios.find(
          (f) => f._id == v.approve_anticipos
        );
        const approve_permisos = usuarios.find(
          (f) => f._id == v.approve_permisos
        );
        const approve_prestamos = usuarios.find(
          (f) => f._id == v.approve_prestamos
        );
        const approve_vacaciones = usuarios.find(
          (f) => f._id == v.approve_vacaciones
        );
        return {
          ...v,
          jefe_dpt_label: jefe_dpt
            ? jefe_dpt.name + " " + jefe_dpt.last_name
            : "Sin jefe",
          approve_anticipos: approve_anticipos
            ? approve_anticipos.name + " " + approve_anticipos.last_name
            : "Sin aprobador",
          approve_permisos: approve_permisos
            ? approve_permisos.name + " " + approve_permisos.last_name
            : "Sin aprobador",
          approve_prestamos: approve_prestamos
            ? approve_prestamos.name + " " + approve_prestamos.last_name
            : "Sin aprobador",
          approve_vacaciones: approve_vacaciones
            ? approve_vacaciones.name + " " + approve_vacaciones.last_name
            : "Sin aprobador",
          employees: emps || [],
        };
      });

      // BUSCAR EL DPT DEL EMPLEADO SEGUN EL CONTRATO
      let contrato = await api("usuarios/contratos", url, "GET", {
        conditions: {
          empleado: user,
          $or: [{ liquidado: { $exists: false } }, { liquidado: false }],
        },
      }, request.headers["access-token"]);
      contrato = contrato.data.length ? contrato.data[0] : null;
      let mi_dpt;
      if (contrato) mi_dpt = dpts.find((d) => d._id == contrato.departamento);

      // COMPROBAR SI EL USUARIO ES JEFE DE DPT
      let jefe_dpt = false;
      if (mi_dpt) {
        if (mi_dpt.jefe_dpt === user) jefe_dpt = true;
        else jefe_dpt = false;
      }

      // CONSTRUIR ARBOL
      let tree = mi_dpt;
      if (mi_dpt) {
        if (jefe_dpt) {
          let iterator = 1;
          tree.label = mi_dpt.name;
          tree.children = [];
          let buildTree = (dpt) => {
            if (dpts.length === iterator || !dpt) {
              return;
            }

            let find = dpts.filter((d) => d.parent == dpt._id);
            if (find.length) {
              if (find.length > 1) {
                find = find.forEach((f) => {
                  f.label = f.name;
                  f.children = [];

                  dpt.children.push(f);
                  iterator++;
                  return buildTree(f);
                });
              }
              if (find && find.length == 1) {
                find = find[0];
                find.label = find.name;
                find.children = [];
                dpt.children.push(find);
              }
            } else {
              find = null;
            }

            iterator++;
            return buildTree(find);
          };
          buildTree(tree);
        } else {
          tree.label = mi_dpt.name;
        }
      }

      return response.status(200).json({
        result: true,
        data: tree,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
