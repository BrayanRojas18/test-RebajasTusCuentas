const { Request, Response } = require("express");
const { api } = require("../db/api.js");
const { upload, remove } = require("../storage/storage.js");
const Vacaciones = require("../models/vacaciones.js");
const moment = require("moment");
const multer = require("multer");
const fs = require("fs");
const { MailAprobar } = require("./NotificacionesController.js");

const validar_rol = async (url, company, params, token) => {
  let roles = await api("roles-tipos/roles", url, "GET", {
    conditions: {
      __company__: company,
    },
  }, token);
  roles = roles.data.length ? roles.data : [];

  const index = await api("roles/roles", url, "GET", {
    conditions: {
      year: moment(params.start).year(),
      month: Number(moment(params.start).format("M")),
      rol: roles.find((v) => v.id == "fdm")._id,
      __company__: company,
    },
  }, token);
  if (index.data.length) return true;
  else return false;
};
const find_apro_dpt = async (url, user, token) => {
  let contrato = await api("usuarios/contratos", url, "GET", {
    conditions: {
      empleado: user,
      $or: [{ liquidado: { $exists: false } }, { liquidado: false }],
    },
  });
  contrato = contrato.data.length ? contrato.data[0] : null;
  if (contrato) {
    let dpt = await api("departamentos/departamentos", url, "GET", {
      conditions: {
        _id: contrato.departamento,
      },
    }, token);
    let usuario = null;
    if (dpt.data.length) {
      usuario = await api("usuarios/usuarios", url, "GET", {
        conditions: {
          _id: dpt.data[0].approve_vacaciones,
        },
      });
      usuario = usuario.data.length ? usuario.data[0] : null;
    }

    return {
      exist: dpt.data.length && dpt.data[0].approve_vacaciones ? true : false,
      apro: usuario ? usuario.name + " " + usuario.last_name : "Sin aprobador",
    };
  } else return { exist: false, apro: null };
};
const getCollection = async (path) => {
  return (path || "").split("/")[0];
};
const getDoc = async (path) => {
  return (path || "").split("/")[1];
};
const saveFile = async (data, company, user, id) => {
  const file = `${data.file.split("/")[0]}/${id} - ${data.file.split("/")[1]}`;
  const comprobante = await upload({
    filePath: `./files/${company}/${user.data._id}/${data.file}`,
    destFileName: `files/${company}/${user.data._id}/${file}`,
  });

  // ELIMNAR ARCHIVO DEL API
  fs.access(`files/${company}/${user.data._id}/${data.file}`, (error) => {
    if (!error) {
      fs.unlinkSync(`files/${company}/${user.data._id}/${data.file}`);
    } else {
      console.error("Error occured:", error);
    }
  });

  return comprobante[0].name;
};

// CALCULOS DE VACACIONES
const calcular_vacaciones = async (params) => {
  let valor = 0;
  let dias = 0;

  // BUSCAR PROVISION SEGUN EL PERIODO
  let find = params.data.find((v) => v.periodo == params.periodo);
  if (find) {
    valor = Number(find.provision) || 0;
    dias = Number(find.dias) || 0;
  }

  let val = (valor / dias) * params.days;
  return Number(val.toFixed(2));
};
const calcular_vacaciones_a = async (params) => {
  let valor = 0;
  let dias = 0;

  // BUSCAR PROVISION SEGUN EL PERIODO
  let find = params.data.find((v) => v.periodo == params.periodo);
  if (find) {
    valor = Number(find.provision_adicional) || 0;
    dias = Number(find.dias_adicionales) || 0;
  }

  let val = (valor / dias) * params.days_adicionales;
  return val;
};

module.exports = {
  // --------------------------------//
  Solicitar: async (request = Request, response = Response) => {
    try {
      const { user, company, parent_company, url, data, file } = request.body;
      // VALIDAR SI HAY ROL GENERADO SEGUN LA FECHA DE SOLICITUD
      const g_rol = await validar_rol(url, company, data, request.headers["access-token"]);
      if (g_rol)
        return response.status(403).json({
          error:
            "Hay rol generado según la fecha de las vacaciones, por lo tanto no se puede generar la solicitud. ",
          status: 403,
        });
      // BUSCAR APROBADOR DE DEPARTAMENTO
      const apro_dpt = await find_apro_dpt(url, user.data._id, request.headers["access-token"]);

      const dias_end = Number(data.days) + (Number(data.days_adicionales) || 0);
      const end = moment(data.start).add(dias_end, "day");

      let valor = 0;
      let valor_adicional = 0;
      if (data.days && data.days > 0) {
        valor = await calcular_vacaciones(data);
      }
      if (data.days_adicionales && data.days_adicionales > 0) {
        if (data.mode_adicional == "norm_adicional") {
          valor_adicional = await calcular_vacaciones_a(data);
        }
        if (data.mode_adicional == "pago_descuento_adicional") {
          valor_adicional = await calcular_vacaciones_a(data);
        }
        if (data.mode_adicional == "days_adicional") {
          valor_adicional = 0;
        }
        if (data.mode_adicional == "pago_adicional") {
          valor_adicional = await calcular_vacaciones_a(data);
          data.days_adicionales = 0;
        }
      }

      let form = {
        ...data,
        valor: valor,
        valor_adicional: valor_adicional,
        end: end,
        site: "portal",
        estatus: "p",
        __company__: company,
      };

      let doc;
      if (apro_dpt.exist) {
        delete form.data;
        if (data._id) {
          if (data.file && file) {
            form["file"] = await saveFile(data, company, user, data._id);
          }
          await Vacaciones.updateOne({ _id: data._id }, form);
        } else {
          doc = await Vacaciones.create(form);

          // CARGAR ARCHIVO SI ES QUE HAY
          if (data.file && file) {
            const comprobante = await saveFile(data, company, user, doc._id);

            await Vacaciones.updateOne({ _id: doc._id }, { file: comprobante });
          }
        }

        // SEND NOTIFICATION
        await MailAprobar(user, company, parent_company, url, "vacaciones");
      } else {
        if (data._id) {
          form._id = data._id;
          if (data.file && file) {
            form["file"] = await saveFile(data, company, user, data._id);
          }
        }
        form.employee = user.data._id;
        const res = await api("vacaciones/solicitar", url, "POST", {
          config: { company, parent_company },
          form,
        }, request.headers["access-token"]);

        if (!data._id) {
          if (data.file && file) {
            form["file"] = await saveFile(data, company, user, res.data._id);
            form._id = res.data._id;
            await api("vacaciones/solicitar", url, "POST", {
              config: { company, parent_company },
              form,
            }, request.headers["access-token"]);
          }
        }
      }
      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  getListTotal: async (request = Request, response = Response) => {
    try {
      const { user, url, company } = request.body;

      let vacaciones = await api(
        "vacaciones/get-vacaciones-list-total",
        url,
        "POST",
        {
          user: user,
          config: { company: company },
        }, request.headers["access-token"]
      );
      vacaciones = vacaciones.data ? vacaciones.data : {};
      return response.status(200).json({
        result: true,
        data: vacaciones,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  getList: async (request = Request, response = Response) => {
    try {
      const { user, url, company } = request.body;

      let vacaciones = await api(
        "vacaciones/get-vacaciones-list",
        url,
        "POST",
        {
          id: user,
          config: { company: company },
        }, request.headers["access-token"]
      );
      vacaciones = vacaciones.data ? vacaciones.data : [];
      return response.status(200).json({
        result: true,
        data: vacaciones,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  MiAprobador: async (request = Request, response = Response) => {
    try {
      const { user, url, company } = request.body;
      let aprobador = await api("vacaciones/mi-aprobador", url, "POST", {
        user,
        config: { company: company },
      }, request.headers["access-token"]);
      return response.status(200).json({
        result: true,
        data: aprobador.data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Solicitudes: async (request = Request, response = Response) => {
    try {
      const { user, company, url, type } = request.body;
      // Pentientes o Rechazadas por Aprobador de DPT
      let vacaciones_dpt = await Vacaciones.find({
        employee: user._id,
        $or: [{ estatus: "p" }, { estatus: "r" }],
        __company__: company,
      });
      //BUSCAR VACACIONES SOLICITADOS EN EL API EXTERNO
      const solicitudes = await api("vacaciones/solicitudes", url, "POST", {
        config: { company },
        data: { user: user.data._id, type, vacaciones_dpt: vacaciones_dpt },
      }, request.headers["access-token"]);
      let data = solicitudes.data.sort(
        (a, b) =>
          moment(b.fecha, "DD-MM").unix() - moment(a.fecha, "DD-MM").unix()
      );
      data = data.map(v => {
        return {
          ...v,
          start: moment(v.start).format('YYYY-MM-DD'),
          end: moment(v.end).format('YYYY-MM-DD'),
        }
      })

      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Activos: async (request = Request, response = Response) => {
    try {
      const { user, company, url, type } = request.body;
      //BUSCAR VACACIONES ACTIVAS
      const activos = await api("vacaciones/activos-pagados", url, "POST", {
        config: { company },
        data: { user: user.data._id, type },
      }, request.headers["access-token"]);
      const data = [...activos.data];
      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Historico: async (request = Request, response = Response) => {
    try {
      const { user, id, company, url } = request.body;
      // Pentientes o Rechazadas por Aprobador de DPT
      let vacaciones_dpt = await Vacaciones.find({
        vacaciones_id: id,
      });
      const historico = await api("vacaciones/historico", url, "POST", {
        config: { company },
        data: { user: user.data._id, id, vacaciones_dpt: vacaciones_dpt },
      }, request.headers["access-token"]);
      const data = historico.data.sort(
        (a, b) =>
          moment(b.fecha, "DD-MM").unix() - moment(a.fecha, "DD-MM").unix()
      );
      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  GetOne: async (request = Request, response = Response) => {
    try {
      const { collection, company, parent_company, url } = request.body;
      const path = await getCollection(collection);
      let vacacion = null;
      if (path === "vacaciones") {
        const doc = await getDoc(collection);
        vacacion = await Vacaciones.findById(doc).exec();
      } else {
        vacacion = await api("vacaciones/get-vacacion", url, "POST", {
          config: { company, parent_company },
          collection,
        }, request.headers["access-token"]);
        vacacion = vacacion.data ? vacacion.data : null;
      }

      if(vacacion) {
        vacacion.start = moment(vacacion.start).format('YYYY-MM-DD')
        vacacion.end = moment(vacacion.end).format('YYYY-MM-DD')
      }

      return response.status(200).json({
        result: true,
        data: vacacion,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Eliminar: async (request = Request, response = Response) => {
    try {
      const { collection, company, parent_company, url, file } = request.body;
      const path = await getCollection(collection);
      if (path === "vacaciones") {
        const doc = await getDoc(collection);
        await Vacaciones.deleteOne({ _id: doc });
      } else {
        await api("vacaciones/eliminar", url, "POST", {
          config: { company, parent_company },
          collection,
        }, request.headers["access-token"]);
      }
      if (file) {
        await remove({
          fileName: file,
        });
      }
      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  UploadFile: async (request = Request, response = Response) => {
    const req = request;
    const res = response;
    try {
      let uploadedFileName = "";
      const storage = multer.diskStorage({
        destination: async function (req, file, cb) {
          let dirPath = `files/${req.query.company}/${req.query.user}/comprobantes-vacaciones`;
          if (!fs.existsSync(dirPath)) {
            await fs.mkdirSync(dirPath, { recursive: true });
          }
          cb(null, dirPath + "/");
        },
        filename: async function (req, file, cb) {
          const fileName = file.originalname;
          uploadedFileName = fileName;
          cb(null, fileName);
        },
      });

      let upload_ = multer({
        storage: storage,
      }).array("file", 12);
      upload_(req, res, function (err) {
        if (err) {
          return res.status(403).json({
            error: "Error al cargar archivo. ",
            status: 403,
          });
        } else {
          return res.status(200).json({
            result: true,
            data: uploadedFileName,
          });
        }
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  RemoveFile: async (request = Request, response = Response) => {
    try {
      const { file, collection, company, parent_company, url, data } =
        request.body;

      const path = await getCollection(collection);
      const doc = await getDoc(collection);

      // VALIDAR SI HAY ROL GENERADO SEGUN LA FECHA DE SOLICITUD
      const g_rol = await validar_rol(url, company, data, request.headers["access-token"]);
      if (g_rol)
        return response.status(403).json({
          error:
            "Hay rol generado según la fecha de las vavaciones, por lo tanto no se puede realizar algún cambio. ",
          status: 403,
        });

      if (path === "vacaciones") {
        await Vacaciones.updateOne({ _id: doc }, { file: null });
      } else {
        await api("vacaciones/editar", url, "POST", {
          config: { company, parent_company },
          collection,
          form: { file: null },
        }, request.headers["access-token"]);
      }

      await remove({
        fileName: file,
      });
      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
