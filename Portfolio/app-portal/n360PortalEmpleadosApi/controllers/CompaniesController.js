const bcrypt = require("bcrypt");
const { Request, Response } = require("express");
const Usuarios = require("../models/usuarios.js");
const Companies = require("../models/companies.js");

module.exports = {
  List: async (request = Request, response = Response) => {
    try {
      let data = [];
      const empresas = await Companies.find();
      if (empresas.length) {
        for (let i in empresas) {
          const company = empresas[i];

          const users = await Usuarios.find({
            __company__: company._id,
          });

          data.push({
            name: company.name,
            users: users.length,
            active: company.active,
            _id: company._id,
          });
        }
      }
      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  GetOne: async (request = Request, response = Response) => {
    try {
      const data = request.body;

      const company = await Companies.findById(data._id).exec();
      const user = await Usuarios.findById(company.admin).exec();

      const form = {
        ...company._doc,
        email: user.email,
        password: user.password,
      };
      return response.status(200).json({
        result: true,
        data: form,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Create: async (request = Request, response = Response) => {
    try {
      const data = request.body;

      // GUARDAR USUARIO ADMIN //

      // BUSCAR Y VALIDAR SI EL USUARIO EXISTE
      const usuario = await Usuarios.findOne({
        email: data.email,
      });
      if (usuario)
        return response.status(403).json({
          error: "El correo electrónico ingresado ya se encuentra registrado. ",
          status: 403,
        });

      const saltRounds = 10;
      const encrypted_password = await bcrypt.hashSync(
        data.password,
        saltRounds
      );
      //CREAR USUARIO
      const user_create = await Usuarios.create({
        email: data.email,
        password: encrypted_password,
        active: true,
        role: "admin",
      });

      // CREAR EMPRESA
      const company = await Companies.create({
        name: data.name,
        active: true,
        admin: user_create._id,
        apiUrl: data.apiUrl,
      });

      // ACTUALIZAR ADMIN
      await Usuarios.updateOne(
        { _id: user_create._id },
        {
          __company__: company._id,
        }
      );

      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Delete: async (request = Request, response = Response) => {
    try {
      const data = request.body;

      await Companies.deleteOne({ _id: data.id });

      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Update: async (request = Request, response = Response) => {
    try {
      let data = request.body;

      // GUARDAR USUARIO ADMIN //

      // BUSCAR Y VALIDAR SI EL USUARIO EXISTE
      const usuario = await Usuarios.findOne({
        email: data.email,
      });
      if (usuario && data.new_email)
        return response.status(403).json({
          error: "El correo electrónico ingresado ya se encuentra registrado. ",
          status: 403,
        });

      const saltRounds = 10;
      let encrypted_password;
      if (data.new_password)
        encrypted_password = await bcrypt.hashSync(data.password, saltRounds);
      else encrypted_password = data.password;

      delete data.new_email;
      delete data.new_password;

      //ACTUALIZAR USUARIO
      await Usuarios.updateOne(
        { _id: data.admin },
        {
          email: data.email,
          password: encrypted_password,
        }
      );

      // ACTUALIZAR EMPRESA
      await Companies.updateOne(
        { _id: data._id },
        {
          ...data,
        }
      );

      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  UpdateStatus: async (request = Request, response = Response) => {
    try {
      let data = request.body;

      // ACTUALIZAR EMPRESA
      await Companies.updateOne(
        { _id: data._id },
        {
          active: data.active,
        }
      );

      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
