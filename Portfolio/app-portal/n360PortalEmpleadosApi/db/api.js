const axios = require("axios");

module.exports = {
  api: async (url, api_name, method, data, token) => {
    return new Promise((resolve, reject) => {
      url = `${api_name}/${url}`;
      
      let object
      if(token) {
        object = {
          method,
          url,
          data: data,
          headers: {
            'access-token': token
          }
        }
      } else {
        object = {
          method,
          url,
          data: data
        }
      }
      
      axios(object)
        .then((data) => {
          resolve(data.data);
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    });
  },
};
