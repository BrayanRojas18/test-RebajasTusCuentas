const env = require("../env");
const { connect } = require("mongoose");

module.exports = {
  connectDB: async (callback) => {
    await connect(env.mongodbUrl);
  },
};
