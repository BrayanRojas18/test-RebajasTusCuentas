let numbers = []
const sum = 24

for(let num = 1; num <= 1000000; num++) {
    if (num % 2 == 0) {
        numbers.push(num)
    }
}

function FindPair() {
    for(let i=0; i<numbers.length-2; i++){
        for(let j=i+1; j<=numbers.length-1; j++){
            if((numbers[i]+numbers[j]) === sum){
                return `(${numbers[i]} + ${numbers[j]}= ${sum})`;
            }
        }
    }
}

console.log('The pair numbers: ', FindPair())

